function addElementInList(text) {
    var item = text.trim();

    var list = document.getElementById('item-list');

    if(item.length > 0) {
        var node = document.createElement("li");
        node.className += "list-group-item";
        var textnode = document.createTextNode(item);
        node.appendChild(textnode);
        list.appendChild(node);
    }
}

function showOutput(json) {
    document.getElementById('item-list').innerHTML = "";
    animals = JSON.parse(json);
    for(var loop_index = 0; loop_index < animals.length; loop_index++) {
        addElementInList(animals[loop_index].name);
    }
}

function searchAnimals() {
    var animal = document.forms['search_form']['animal'].value;

    var url="search.php?animal="+animal;
    if(window.XMLHttpRequest){
        request=new XMLHttpRequest();
    }
    else if(window.ActiveXObject){
        request=new ActiveXObject("Microsoft.XMLHTTP");
    }

    try
    {
        request.onreadystatechange=getInfo;
        request.open("GET",url,true);
        request.send();
    }
    catch(e)
    {
        alert("Unable to connect to server");
    }

    return false;
}

var request;

function getInfo(){
    if(request.readyState==4){
        var val=request.responseText;
        if(val !== "" || val !== "error")
            showOutput(val);
    }
}

searchAnimals();
