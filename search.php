<?php

    $host = "localhost";
    $user = "root";
    $pass = "bad9328";
    $db = "testdb";

    $con = new mysqli($host, $user, $pass, $db);


    if($con->connect_errno) {
        die("error");
    }

    if(isset($_GET['animal'])) {
        $animal = trim($_GET['animal']);
        $results;
        if(empty($animal)) {
            $results = $con->query("select * from animals order by name");
        } else {
            $results = $con->query("select * from animals where name like '%$animal%' order by name");
        }

        echo json_encode($results->fetch_all(MYSQLI_ASSOC));
    }

?>
